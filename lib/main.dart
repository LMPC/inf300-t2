import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import './View/Pages/login_page.dart';
import './Themes/text_themes.dart';
import './Model/light_model.dart';
import './Languages/localization.dart';


void main() => runApp( MyBlock() );

class MyBlock extends StatelessWidget {
  
  const MyBlock( {Key key} ) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(

      localizationsDelegates: [
        
        AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      
      ],
      
      supportedLocales: [Locale("en"), Locale("es"), Locale("pt")],

      locale: Locale("es"),
      
      theme: ThemeData(
        
        primaryColor: Colors.lightBlue,
        brightness: Brightness.light,
        
        textTheme: TextTheme(
          
          headline1: TextThemes.title,
          subtitle1: TextThemes.subtitle,
          bodyText1: TextThemes.body1,
        
        ),
      
      ),

      debugShowCheckedModeBanner: false,

      home: Provider <LightModel> (
        
        create: (context) => LightModel(),
        
        dispose: (context, _lightModel) => _lightModel.cancel(),
        
        child: LoginPage(),
      
      )
    
    );
  
  }

}