import 'package:flutter/material.dart';

class TextThemes {
  
  static final TextStyle title = TextStyle (
    
    fontSize: 18,
    
    fontWeight: FontWeight.w600,

    color: Colors.lightBlueAccent,
  
  );

  static final TextStyle subtitle = TextStyle (
    
    fontSize: 12,
    
    fontWeight: FontWeight.w500,
  
  );

  static final TextStyle body1 = TextStyle (
    
    fontSize: 14,
    
    fontWeight: FontWeight.w300,
  
  );

  static final TextStyle dateStyle = TextStyle (
    
    fontSize: 12,
    
    fontWeight: FontWeight.w500,
    
    color: Colors.lightBlue,
  
  );

}