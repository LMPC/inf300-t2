import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import './l10n/messages_all.dart';


class AppLocalizations {
  
  static Future <AppLocalizations> load(Locale locale) {
    
    final String name = locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages( localeName ).then( (bool _) {
      
      Intl.defaultLocale = localeName;
      
      return new AppLocalizations();
    
    } );
  
  }

  static AppLocalizations of(BuildContext context) {
    
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  
  }

  String get block {
    
    return Intl.message('BLOCK', name: 'block');
  
  }

  String get issue {
    
    return Intl.message('ISSUE', name: 'issue');
  
  }

  String get anonymous {
    
    return Intl.message('anonymous', name: 'anonymous');
  
  }

  String get comments {
    
    return Intl.message('comments', name: 'comments');
  
  }

}

class AppLocalizationsDelegate extends LocalizationsDelegate <AppLocalizations> {
  
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    
    return ['en', 'es', 'pt'].contains(locale.languageCode);
  
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    
    return AppLocalizations.load(locale);
  
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) {
    
    return false;
  
  }

}