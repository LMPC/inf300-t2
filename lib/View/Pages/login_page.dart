import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import '../../Model/light_model.dart';
import '../../Util/common.dart';
import './home_page.dart';



class LoginPage extends StatefulWidget {
  
  LoginPage( {Key key} ) : super( key: key );

  @override
  _LoginPageState createState() => _LoginPageState();
}


class _LoginPageState extends State <LoginPage> {
  
  TextStyle style = TextStyle( 
    
    fontFamily: 'Montserrat', 
    
    fontSize: 20.0, 
    
    color: Colors.blueGrey,
    
  );

  
  @override
  Widget build(BuildContext context) {
    
    final _lightModel = Provider.of<LightModel>(context);

    final _emailField = TextField(
      
      obscureText: false,
      style: style,
      decoration: InputDecoration(
        
        contentPadding: EdgeInsets.fromLTRB( 20.0, 15.0, 20.0, 15.0 ),
        hintText: "Email",
        labelText: "Enter email",
        border: OutlineInputBorder( borderRadius: BorderRadius.circular(32.0),),     
      ),

      keyboardType: TextInputType.emailAddress,
      
    );

    
    final _passwordField = TextField(
      
      obscureText: true,
      style: style,
      
      decoration: InputDecoration(

        contentPadding: EdgeInsets.fromLTRB( 20.0, 15.0, 20.0, 15.0 ),
        hintText: "Password",
        labelText: "Enter password",
        border: OutlineInputBorder( borderRadius: BorderRadius.circular( 32.0 ) )
        
      ),
    
    );

    final _loginButton = Material(
      
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xff01A0C7),
      
      child: MaterialButton(
        
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        
        onPressed: () {

          Navigator.push( 
          
            context,
          
            MaterialPageRoute( builder: ( BuildContext context ) {
          
              return HomePage();
          
            } )

          );

        },
        
        child: Text("Login",
          
          textAlign: TextAlign.center,
          
          style: style.copyWith(
            
            color: Colors.white, fontWeight: FontWeight.bold)),
      
      ),
    
    );
  
    return Scaffold(
        
        body: StreamBuilder <double> (
            
          initialData: _lightModel.lightValue,
            
          stream: _lightModel.lightStream,
            
          builder: (context, snapshot) => Container(
              
            width: double.infinity,
            height: double.infinity,
            color: Color.lerp(Colors.black, Colors.white, _computeColor(snapshot.data)),
            //child: LightText(value: snapshot.data),

            child: SingleChildScrollView(
              
              child: Center(
                
                child: Padding(
                  
                  padding: const EdgeInsets.all( 36.0 ),
                  
                  child: Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    
                    children: <Widget> [

                      Row(

                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: <Widget> [
                        
                          Icon( 
                            
                            Icons.lightbulb_outline,
                            
                            color: Colors.amber[600],
                          
                          ),

                          Text(
                        
                            snapshot.data.toString (),
                            
                            style: TextStyle(
              
                              color: Colors.amber[600], 
                              fontSize: 20, 
                              fontFamily: DefaultTextStyle.of(context).style.fontFamily,
          
                            ),
        
                          ),

                        ]

                      ), 
                      
                      SizedBox( height: 100.0 ),
                      
                      _emailField,
                      
                      SizedBox( height: 25.0 ),
                      
                      _passwordField,
                      
                      SizedBox( height: 35.0 ),
                      
                      _loginButton,
                      
                      SizedBox( height: 15.0 ),
                    
                    ],
                  
                  ),
                
                ),
        
              ),
            
            )
        
          )
        
        )
        
    );
  
  }

  StreamSubscription _lightSubscription;
  final streamChannel = const EventChannel(PLATFORM_CHANNEL);
  _setLightValue(double value, LightModel _lightModel) => _lightModel.lightSink.add(value);
  _computeColor(double value) => value / 10;

  @override
  void initState() {
    
    super.initState();
    
    _lightSubscription =
        streamChannel.receiveBroadcastStream().listen((data) => _setLightValue(data, Provider.of<LightModel>(context)));
  }

  @override
  void dispose() {
    
    super.dispose();
    
    _lightSubscription.cancel();
  }

}
