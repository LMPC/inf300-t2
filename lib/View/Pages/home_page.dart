import 'package:flutter/material.dart';

import '../../Util/demo_val.dart';
import '../Widgets/post_card.dart';
import '../Widgets/popup_menu.dart';


class HomePage extends StatefulWidget {
  
   _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State <HomePage> {  
  
  
  @override
  Widget build(BuildContext context) {
    
    
    return Scaffold(
      
      appBar: AppBar(
        
        leading: PopupMenu(
          
          icons: [
            
            Icon(Icons.map),
            Icon(Icons.settings),
            Icon(Icons.account_circle),
          
          ],
          
          iconColor: Colors.white,

          backgroundColor: Colors.lightBlue,
          
          onChange: (index) { },
        
        ),
      
        actions: <Widget> [
      
          Padding(
        
            padding: EdgeInsets.only( right: 20.0 ),
        
            child: GestureDetector(
        
              onTap: () {},
        
              child: Icon(
          
                Icons.search,

                color: Colors.white,
        
              ),
        
            )
    
          ),
    
          Padding(
      
            padding: EdgeInsets.only( right: 20.0 ),
      
            child: GestureDetector(
              
              onTap: () { },
        
              child: Icon(
          
                Icons.add,

                color: Colors.white,
        
              ),
      
            )
    
          ),
  
        ],
      
      ),
      
      body: ListView.builder(
        
        itemCount: DemoValues.posts.length,
        
        itemBuilder: ( BuildContext context, int index ) {
          
          return PostCard( postData: DemoValues.posts[index] );
        
        },
      
      ),
    
    );
 
  }

}