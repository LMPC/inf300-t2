import 'package:flutter/material.dart';

import '../../Model/post_model.dart';
import '../../Themes/text_themes.dart';
import '../Widgets/comments_list.dart';
import '../Widgets/InheritedWidgets/inherited_post_model.dart';
import '../Widgets/post_stats.dart';
import '../Widgets/post_time_stamp.dart';
import '../Widgets/user_details.dart';
import '../../Languages/localization.dart';

class PostPageKeys {
  
  static final ValueKey wholePage = ValueKey("wholePage");
  static final ValueKey blockTag = ValueKey("blockTag");
  static final ValueKey issueTag = ValueKey("issueTag");
  static final ValueKey mainBody = ValueKey("mainBody");

}


class PostPage extends StatelessWidget {
  
  final PostModel postData;

  const PostPage( {Key key, @required this.postData} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      
      appBar: AppBar( 
        
      ),
      
      
      body: InheritedPostModel(
        
        postData: postData,
        
        child: ListView(
          
          key: PostPageKeys.wholePage,
          
          children: <Widget> [_NonImageContents()],
        
        ),
      
      ),
    
    );
  
  }

}


class _NonImageContents extends StatelessWidget {
  
  const _NonImageContents( {Key key} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
    
    final PostModel postData = InheritedPostModel.of(context).postData;

    return Container(
      
      margin: const EdgeInsets.all( 8.0 ),
      
      child: Column(
        
        crossAxisAlignment: CrossAxisAlignment.start,
        
        children: <Widget> [

          PostTimeStamp(),

          SizedBox(height: 20),

          Text( AppLocalizations.of(context).block + " "+ postData.hashtag.item1 ),

          SizedBox(height: 5),

          Text( AppLocalizations.of(context).issue + " " + postData.hashtag.item2 ),

          SizedBox(height: 20),
          
          _MainBody( key: PostPageKeys.mainBody ),

          SizedBox(height: 20.0),
          
          Row(
      
            children: <Widget> [
        
              Expanded(
          
                flex: 3, 
                child: UserDetails(userData: postData.author)
          
              ),
        
              Expanded(
          
                flex: 1, 
                child: PostStats()
          
              ),
      
            ],
    
          ),

          SizedBox(height: 10.0),

          CommentsList(),
        
        ],
      
      ),
    
    );
  
  }

}


class _MainBody extends StatelessWidget {
  
  const _MainBody( {Key key} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
   
    return Padding(
      
      padding: const EdgeInsets.symmetric( vertical: 8.0 ),
      
      child: Text(
        
        InheritedPostModel.of(context).postData.postText,
        
        style: TextThemes.body1,
      
      ),
    
    );
  
  }

}