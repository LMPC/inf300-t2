import 'package:flutter/material.dart';


import '../../Model/post_model.dart';
import './InheritedWidgets/inherited_post_model.dart';


class PostStats extends StatelessWidget {
  
  const PostStats( {Key key} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
    
    final PostModel postData = InheritedPostModel.of(context).postData;

    return Row(
      
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      
      children: <Widget> [
        
        _ShowStat(
          
          icon: Icons.check,
          
          number: postData.reacts,
          
          color: Colors.lightBlue,
        
        ),
        
        _ShowStat(
          
          icon: Icons.comment,
     
          number: postData.views,
          
          color: Colors.lightBlue,
        
        ),
      
      ],
    
    );
  
  }

}


class _ShowStat extends StatelessWidget {
  
  final IconData icon;
  final int number;
  final Color color;

  const _ShowStat( {
    
    Key key,
    
    @required this.icon,
    
    @required this.number,
    
    @required this.color,
  
  } ) : super( key: key );


  @override
  Widget build(BuildContext context) {
    
    return Column(
      
      children: <Widget> [
        
        Padding(
          
          padding: const EdgeInsets.only( right: 2.0 ),
          
          child: Icon(icon, color: color),
        
        ),
        
        Text( number.toString( ) ),
      
      ],
    
    );
  
  }

}