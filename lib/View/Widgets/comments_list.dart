import 'package:flutter/material.dart';

import '../../Model/comment_model.dart';
import './InheritedWidgets/inherited_post_model.dart';
import './user_details.dart';

import '../../Languages/localization.dart';


class CommentsListKeyPrefix {
  
  static final String comment = "Comment";
  static final String user = "CommentUser";
  static final String text = "CommentText";
  static final String divider = "CommentDivider";

}


class CommentsList extends StatelessWidget {
  
  const CommentsList( {Key key} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
    
    final List <CommentModel> comments = InheritedPostModel.of(context).postData.comments;

    return Padding(
      
      padding: const EdgeInsets.only( top: 8.0 ),
      
      child: ExpansionTile(
        
        leading: Icon( Icons.comment ),
        
        trailing: Text( comments.length.toString() ),
        
        title: Text( AppLocalizations.of(context).comments ),
        
        children: List <Widget>.generate(
          
          comments.length,
          
          ( int index ) => _SingleComment(
                        
            key: ValueKey ( "${CommentsListKeyPrefix.comment} $index" ),
            
            index: index,
          
          ),
        
        ),
      
      ),
    
    );
  
  }

}


class _SingleComment extends StatelessWidget {
  
  final int index;

  const _SingleComment( {Key key, @required this.index} ) : super( key: key );


  @override
  Widget build(BuildContext context) {
    
    final CommentModel commentData = InheritedPostModel.of(context).postData.comments[index];

    return Container (
      
      width: double.infinity,
      
      margin: const EdgeInsets.symmetric( vertical: 8.0 ),
      
      child: Column(
        
        crossAxisAlignment: CrossAxisAlignment.start,
        
        children: <Widget> [
          
          UserDetails (
            
            key: ValueKey( "${CommentsListKeyPrefix.user} $index"),
            
            userData: commentData.user,
          
          ),
          
          SizedBox(height: 10.0),

          Text(
            
            commentData.comment,
            
            key: ValueKey( "${CommentsListKeyPrefix.text} $index" ),
            
            textAlign: TextAlign.justify,
         
          ),
          
          Divider(
            
            key: ValueKey( "${CommentsListKeyPrefix.divider} $index" ),
            
            color: Colors.black45,
          
          ),
        
        ],
      
      ),
    
    );
  
  }

}