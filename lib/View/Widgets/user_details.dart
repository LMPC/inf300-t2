import 'package:flutter/material.dart';

import '../../Util/common.dart';
import '../../Model/user_model.dart';
import './InheritedWidgets/inherited_user_model.dart';


class UserDetails extends StatelessWidget {
  
  final UserModel userData;

  const UserDetails( {
    
    Key key, 
    @required this.userData
  
  } ) : super( key: key );

  
  @override
  Widget build(BuildContext context) {
    
    return InheritedUserModel(
      
      userData: userData,
      
      child: Container(
        
        child: Row( children: <Widget> [_UserImage( ), _UserName( )] ),
      
      ),
    
    );
  
  }

}


class _UserImage extends StatelessWidget {
  
  const _UserImage( {Key key} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
    
    final UserModel userData = InheritedUserModel.of(context).userData;
    
    return Expanded(
      
      flex: 1,
      
      child: CircleAvatar( backgroundImage: AssetImage( userData.image ) ),
    
    );
  
  }

}


class _UserName extends StatelessWidget {
  
  const _UserName( {Key key} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
    
    final UserModel userData = InheritedUserModel.of( context ).userData;
    final TextStyle nameTheme = Theme.of(context).textTheme.subtitle1;
    
    final int flex = isLandscape( context ) ? 10 : 5;
    
    return Expanded(
      
      flex: flex,
      
      child: Padding(
        
        padding: const EdgeInsets.all( 4.0 ),
        
        child: Text( userData.name, style: nameTheme ),
      
      ),
    
    );
  
  }

}