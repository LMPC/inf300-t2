import 'package:flutter/material.dart';

import '../../Model/post_model.dart';
import './InheritedWidgets/inherited_post_model.dart';
import '../../Util/common.dart';
import './post_stats.dart';
import './post_time_stamp.dart';
import './user_details.dart';
import '../Pages/post_page.dart';


class PostCard extends StatelessWidget {
  
  final PostModel postData;

  const PostCard( { 
    
    Key key, 
    
    @required this.postData
    
  } ) : super( key: key );


  @override
  Widget build(BuildContext context) {
    
    final double aspectRatio = isLandscape(context) ? 6 / 2 : 6 / 3;
    
    return GestureDetector (

      onHorizontalDragEnd: (DragEndDetails details) {

        if(details.primaryVelocity == 0) return;

        if (details.primaryVelocity.compareTo(0) == -1) {
      
          Navigator.push( 
          
            context,
          
            MaterialPageRoute( builder: ( BuildContext context ) {
          
              return PostPage( postData: postData );
          
            } )
          
          );
                    
          //print('dragged from left');

        } else {

          //print('dragged from right');

        }

      },
      
      child: AspectRatio(
        
        aspectRatio: aspectRatio,
        
        child: Card(
          
          elevation: 2,
          
          child: Container(
            
            margin: const EdgeInsets.all( 4.0 ),
            padding: const EdgeInsets.all( 4.0 ),
            
            child: InheritedPostModel(
              
              postData: postData,
              
              child: Column(
                
                children: <Widget> [
                  
                  _Post(),
                  
                  Divider(color: Colors.grey),
                  
                  _PostDetails(),
                
                ],
              
              ),
            
            ),
          
          ),
        
        ),
      
      ),
    
    );
  
  }

}


class _Post extends StatelessWidget {
  
  const _Post( {Key key} ) : super( key: key );

  @override
  Widget build(BuildContext context) {
    
    return Expanded(
      
      flex: 2,
      
      child: Row( children: <Widget> [_PostTitleBodyAndTime( )] ),
    
    );
  
  }

}

class _PostTitleBodyAndTime extends StatelessWidget {
  
  const _PostTitleBodyAndTime ( {Key key} ) : super ( key: key );

  @override
  Widget build(BuildContext context) {
    
    final PostModel postData = InheritedPostModel.of(context).postData;
    
    final TextStyle tagTheme = Theme.of(context).textTheme.headline1;
    final TextStyle bodyTheme = Theme.of(context).textTheme.bodyText1;
    
    final String blocktag = postData.hashtag.item1;
    final String issuetag = postData.hashtag.item2;
    final String bodyText = postData.postText;
  
    final int flex = isLandscape(context) ? 5 : 3;

    return Expanded(
      
      flex: flex,
      
      child: Padding(
        
        padding: const EdgeInsets.only(left: 4.0),
        
        child: Column(
          
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          
          children: <Widget> [
            
            Column(
              
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              
              children: <Widget> [
                
                Wrap(
                  
                  spacing: 10,
                  
                  children: <Widget> [
                    
                    Text( blocktag, style: tagTheme ),

                    Text( issuetag, style: tagTheme ),
                  
                  ],
                
                ),
                           
                SizedBox( height: 10 ),
                
                Text( 
                  
                  bodyText,
                  
                  style: bodyTheme,
                  
                  textAlign: TextAlign.justify,

                  overflow: TextOverflow.ellipsis,
                  
                  maxLines: 3, 
                
                ),
              
              ],
            
            ),
            
            PostTimeStamp( alignment: Alignment.centerRight ),
          
          ],
        
        ),
      
      ),
    
    );
  
  }

}

class _PostDetails extends StatelessWidget {
  
  const _PostDetails ( {Key key} ) : super ( key: key );

  @override
  Widget build(BuildContext context) {
    
    final PostModel postData = InheritedPostModel.of(context).postData;

    return Row(
      
      children: <Widget> [
        
        Expanded(
          
          flex: 3, 
          child: UserDetails(userData: postData.author)
          
        ),
        
        Expanded(
          
          flex: 1, 
          child: PostStats()
          
        ),
      
      ],
    
    );
  
  }

}