import 'package:flutter/material.dart';

class UserModel {
  
  final String id;
  final String email;
  final String name;
  final String image;
  
  const UserModel( {
   
    @required this.id,
    @required this.email,
    @required this.name,
    @required this.image,
   
  } );

}