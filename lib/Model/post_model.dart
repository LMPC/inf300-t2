import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tuple/tuple.dart';

import './comment_model.dart';
import './user_model.dart';


class PostModel {
  
  final String id;
  final Tuple2 < String, String > hashtag;
  final String postText;
  final DateTime postTime;
  
  final UserModel author;
  final int views, reacts;
  
  final List < CommentModel > comments;

  const PostModel( {
    
    @required this.id,
    @required this.hashtag,
    @required this.postText,
    @required this.postTime,
    
    @required this.author,
    @required this.reacts,
    @required this.views,
    
    @required this.comments,
  
  } );

  String get postTimeFormatted => DateFormat.yMMMMEEEEd( ).format( postTime );

}