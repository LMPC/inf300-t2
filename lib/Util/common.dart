import 'package:flutter/material.dart';

bool isLandscape(BuildContext context) =>
  (MediaQuery.of( context ).orientation == Orientation.landscape);


const PLATFORM_CHANNEL = "com.example.light/platform";