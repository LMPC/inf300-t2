import 'package:tuple/tuple.dart';

import '../Model/comment_model.dart';
import '../Model/post_model.dart';
import '../Model/user_model.dart';


class DemoValues {
  
  static final List <UserModel> users = <UserModel> [
    
    UserModel(
      
      id: "1",
      name: "xxxx",
      email: "anonymous1@gmail.com",
      image: "assets/images/anonymous.png",
    
    ),
    
    UserModel(
      
      id: "2",
      name: "xxxx",
      email: "anonymous2@gmail.com",
      image: "assets/images/anonymous.png",
    
    ),
    
    UserModel(
      
      id: "3",
      name: "xxxx",
      email: "anonymous3@gmail.com",
      image: "assets/images/anonymous.png",
    
    ),
  
  ];

  static final List <CommentModel> _comments = <CommentModel> [
    
    CommentModel(
      
      comment: "Et hic et seoul inventors. Molestiae laboriosam commodi exercitationem eum. ",
      user: users[0],
      time: DateTime( 2020, 4, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Unde id provident ut sunt in consequuntur qui sed. ",
      user: users[1],
      time: DateTime( 2020, 5, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Eveniet nesciunt distinctio sint ut. ",
      user: users[0],
      time: DateTime( 2020, 6, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Et facere a eos accusantium culpa quaerat in fugiat suscipit.",
      user: users[2],
      time: DateTime ( 2020, 4, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Necessitatibus pariatur harum deserunt cum illum ut.",
      user: users[1],
      time: DateTime( 2020, 5, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Accusantium neque quis provident voluptatem labore quod.",
      user: users[2],
      time: DateTime( 2020, 6, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Accusantium neque quis provident voluptatem labore quod.",
      user: users[1],
      time: DateTime( 2020, 4, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Neque est ut rerum vel sunt harum voluptatibus et.",
      user: users[0],
      time: DateTime( 2020, 5, 30 ),
    
    ),
    
    CommentModel(
      
      comment: "Hic accusantium minus fuga exercitationem id aut expedita doloribus.",
      user: users[1],
      time: DateTime( 2020, 6, 30 ),
    
    ),
  
  ];

  static final List <PostModel> posts = <PostModel> [
    
    PostModel(
      
      id: "1",
      author: users[0],
      hashtag: Tuple2 < String, String > ( "#andaraí", "#tiroteio" ),
      postText: "Disparos no Morro do Cruz, Complexo do Andaraí. Cuidado na região!",
      postTime: DateTime( 2020, 06, 09, 1, 41 ),
      
      reacts: 125,
      views: 9,
      comments: _comments,
    
    ),
    
    PostModel(
      
      id: "2",
      author: users[1],
      hashtag: Tuple2 < String, String > ( "#copacabana", "#interdição" ),
      postText: "Avenida Atlântica segue com bloqueios parciais, sentido Leme, devido a uma manifestação na altura do posto 5",
      postTime: DateTime( 2020, 05, 31, 12, 15 ),
      
      reacts: 231,
      views: 9,
      comments: _comments,
    
    ),

    PostModel(
      
      id: "3",
      author: users[2],
      hashtag: Tuple2 < String, String > ( "#novaiguaçu", "#incêndio" ),
      postText: "Carro pegando fogo na Dutra, sentido Rio, depois do Atacadão, na pista marginal. Bombeiros no local.",
      postTime: DateTime( 2020, 05, 23, 11, 32 ),
      
      reacts: 432,
      views: 9,
      comments: _comments,
    
    ),

    PostModel(
      
      id: "4",
      author: users[0],
      hashtag: Tuple2 < String, String > ( "#pavuna", "#tiroteio" ),
      postText: "Tiros no Complexo do Chapadão, localidade Caminho do Padre, Pavuna. Cuidado na região.",
      postTime: DateTime( 2020, 05, 16, 09, 37 ),
      
      reacts: 120,
      views: 9,
      comments: _comments,
    
    ),

    PostModel(
      
      id: "5",
      author: users[1],
      hashtag: Tuple2 < String, String > ( "#cidadededeus", "#tiroteio" ),
      postText: "Tiros na Cidade de Deus, localidade Tangará. Operação policial. Houve confrontos mais cedo na Josias e Tijolinho.",
      postTime: DateTime( 2020, 05, 08, 08, 05 ),
      
      reacts: 224,
      views: 9,
      comments: _comments,
    
    ),
  
  ];

}